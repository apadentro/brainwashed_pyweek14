﻿#-------------------------------------------------------------------------------
# Name:        module1
# Purpose:
#
# Author:      Carlos
#
# Created:     06/05/2012
# Copyright:   (c) Carlos 2012
# Licence:     <your licence>
#-------------------------------------------------------------------------------
#!/usr/bin/env python

import shelve
import os
import pygame._view #for cx_freeze
from dbm import dumb #for cx_freeze
import pygame, sys, math, time, random
from pygame.color import THECOLORS
from pygame.locals import *
import cProfile

class GameConfig:
    def __init__(self):
        self.state = None
        self.width = None
        self.height = None
        self.fps = None
        self.screen = None

class MissionConfig:
    def __init__(self):
        pass

class Plane:
    ACCELERATION = 0.2
    TURN_SPEED = 12
    TOP_SPEED = 10
    MIN_SPEED = 6

    def __init__(self, imageName='airplane.png'):

        self.bulletSize = 3
        self.timeoutside = 0
        self.bombReady = True
        self.bombs = 0
        self.exploded=False

        self.src_image = pygame.image.load(
                                        datafilepath(imageName)).convert_alpha()
        self.rect = self.src_image.get_rect()
        #self.src_image = pygame.transform.rotate(self.src_image, -90)
        self.src_image = resizeImage(self.src_image,1)

        self.position = (200, 50)
        self.speed = 0
        self.direction = -90
        self.acc = 0
        self.turn = 0

    def update(self,playerInput='true'):
        if playerInput:
            pygame.event.pump()
            pressed = pygame.key.get_pressed()
            self.turn = 0
            if pressed[pygame.K_RIGHT] or pressed[pygame.K_d]:
                self.turn -= self.TURN_SPEED
            if pressed[pygame.K_LEFT] or pressed[pygame.K_a]:
                self.turn += self.TURN_SPEED
            if pressed[pygame.K_UP] or pressed[pygame.K_w]:
                self.acc += self.ACCELERATION
            else:
                self.acc = 0
            if pressed[pygame.K_DOWN] or pressed[pygame.K_s]:
                self.acc -= self.ACCELERATION*3

        self.speed += self.acc
        if self.speed > self.TOP_SPEED:
            self.speed = self.TOP_SPEED
        if self.speed < self.MIN_SPEED:
            self.speed = self.MIN_SPEED
        self.direction += self.turn
        x, y = self.position
        rad = self.direction * math.pi / 180
        x += -self.speed * math.sin(rad)
        y += -self.speed * math.cos(rad)
        self.position = (x, y)
        if playerInput:
            self.image = pygame.transform.rotate(self.src_image, self.direction)
        else:
            self.image=self.src_image
        imgRect = self.image.get_rect()
        self.rect = pygame.Rect(self.position, (imgRect.width,imgRect.height))

class Bomb:
    TOP_SPEED = 9.8
    ACCELERATION = 0.07
    def __init__(self, position):
        self.image = pygame.image.load(datafilepath('bomb.png')).convert_alpha()
        self.image = pygame.transform.rotate(self.image, -90)
        self.image = resizeImage(self.image,0.15)
        self.rect = self.image.get_rect()

        self.position = position
        self.speed = 0
        self.acc = 0

    def update(self):
        self.acc += self.ACCELERATION
        self.speed += self.acc
        if self.speed > self.TOP_SPEED:
            self.speed = self.TOP_SPEED

        x, y = self.position
        y += self.speed
        self.position = (x, y)
        imgRect = self.image.get_rect()
        self.rect = pygame.Rect(self.position, (imgRect.width,imgRect.height))

class Human:
    TOP_SPEED = 10
    MIN_SPEED = 4
    ACCELERATION = 0.2
    TURN_SPEED = 20

    def __init__(self, pic):
        self.pic = pic
        self.infected=False
        self.updateImage()
        self.position = (random.randint(100,700),random.randint(500,570))
        self.speed = 0
        self.direction = -90
        self.acc = 0
        self.turn = 0
        self.movex = 0
        self.movey = 0

    def updateImage(self):
        if not self.infected:
            self.image = pygame.image.load(datafilepath(self.pic)).convert_alpha()
        else:
            self.image = pygame.image.load(
                                    datafilepath(self.pic.replace(".","v."))).convert_alpha()

        self.image = resizeImage(self.image,random.uniform(0.4,1))

    def update(self, ground):
        if not self.infected:
            if random.randint(0,50)==9:
                multi = 1.5
                self.movex = random.randint(-2,+2)*multi
                self.movey = random.randint(-2,+2)*multi
            else:
                x, y = self.position
                x += self.movex
                y += self.movey
                if  (0+20 <x <800-20) and (500<y<600-20):
                    self.position = (x, y)

            imgRect = self.image.get_rect()
            self.rect = pygame.Rect(self.position,
                                    (imgRect.width,imgRect.height))

class Cloud:
    IMAGE = None
    def __init__(self):
        if Cloud.IMAGE is None:
            self.image = pygame.image.load(datafilepath('cloud.png')).convert_alpha()
        x = random.randint(-10,800)
        y = random.randint(-10,450)
        self.position = x,y

class Spill:
    def __init__(self, position):
        self.image = pygame.image.load(datafilepath('spill.png')).convert_alpha()
        self.createTime = time.time()
        self.image = resizeImage(self.image,0.8)
        width=self.image.get_rect().width
        height=self.image.get_rect().height
        xb, yb = position
        x = xb-width/2
        y = yb-height/2
        self.position = x,y
        self.rect = pygame.Rect(self.position, (width,height))

class Blood:
    def __init__(self, position):
        self.image = pygame.image.load(datafilepath('blood.png')).convert_alpha()
        self.image = resizeImage(self.image,0.2)
        width=self.image.get_rect().width
        height=self.image.get_rect().height
        xb, yb = position
        x = xb-width/2
        y = yb-height/2
        self.position = x,y
        self.rect = pygame.Rect(self.position, (width,height))

class Explosion:
    IMAGES = []
    def __init__(self,position,multi=1):
        self.createTime = time.time()
        self.images = []
        if len(Explosion.IMAGES) <1: #caching images (loaded just once)
            for i in range(1,6):
                pic =pygame.image.load(datafilepath('ex'+str(i)+'.png')).convert_alpha()
                pic = resizeImage(pic,multi)
                Explosion.IMAGES.append(pic)
        self.images = Explosion.IMAGES
        self.currentImage = 0
        self.image = self.images[self.currentImage]
        width = self.image.get_rect().width
        height= self.image.get_rect().height
        xb, yb = position
        x = xb-width/2
        y = yb-height/2
        self.position = x,y
        self.rect = pygame.Rect(self.position, (width,height))
    def update(self,explosions):
        if time.time() > self.createTime +0.07:
            self.createTime = time.time()
            self.currentImage += 1
            if not self.currentImage < len(self.images):
                explosions.remove(self)
            else:
                self.image = self.images[self.currentImage]

class Bullet:
    def __init__(self, position, direction,size=3,color='red',fromPlayer=False):
        self.fromPlayer = fromPlayer
        self.color = color
        self.size = size
        self.speed = 15
        self.position = position
        self.direction = direction
        self.rect = pygame.Rect(position,(self.size,self.size))
    def update(self):
        x, y = self.position
        rad = self.direction * math.pi / 180
        x += -self.speed * math.sin(rad)
        y += -self.speed * math.cos(rad)
        self.position = (x, y)
        self.rect = pygame.Rect(self.position,(self.size,self.size))

class Tank:
    TOP_SPEED = 0.5
    ACCELERATION = 0.005

    def __init__(self,posLeft, posRight):
        self.createTime = time.time()
        self.bombReady = True
        self.bombs = 6
        self.exploded=False

        xleft=random.randint(-10,250)
        xright= random.randint(550,820)
        x=0
        if posLeft and posRight:
            x=random.choice([xleft,xright])
        elif posRight:
            x=xright
        elif posLeft:
            x=xleft

        self.flip=False
        if x==xright:
            self.flip= True

        self.position = (x, random.randint(450,500))

        self.images = self.loadImages()
        self.src_image = self.images[0]
        self.rect = self.src_image.get_rect()

        self.speed = 0
        self.acc = 0
        self.turn = 0
        self.sentido = -1

    def update(self,bullets):
        if time.time()>self.createTime+2:
            self.src_image = self.images[1]
        if time.time()>self.createTime+4:
            self.src_image = self.images[2]
        if time.time()>self.createTime+6:
            if random.randint(0,40)==1:
                x,y = self.rect.center
                way=-1
                if self.flip:
                    way=1
                bullets.append(Bullet((x,y-20),
                                    random.randint(5,25)*way,5,'orange'))
        self.acc += self.ACCELERATION
        self.speed += self.acc

        if self.speed > self.TOP_SPEED:
            self.speed = self.TOP_SPEED
        if self.speed < 0:
            self.speed = 0
        if random.randint(0,300)==1:
            self.sentido *= -1
        x, y = self.position
        x += self.speed * self.sentido
        if x <0:
            x = 0
        elif x>800-self.rect.width:
            x=800-self.rect.width
        y = y
        self.position = (x, y)
        self.image = self.src_image
        imgRect = self.image.get_rect()
        self.rect = pygame.Rect(self.position, (imgRect.width,imgRect.height))

    def loadImages(self):
        images = []
        for i in range(1,4):
            tankname='tank'+str(i)+'.png'
            image = pygame.image.load(datafilepath(tankname)).convert_alpha()
            image = resizeImage(image,0.5)
            if not self.flip:
                image = pygame.transform.flip(image, True, False)
            images.append(image)
        return images

class Helicopter:
    TOP_SPEED = 0.5
    ACCELERATION = 0.005

    def __init__(self,posLeft,posRight):
        self.createTime = time.time()
        self.bombReady = True
        self.bombs = 6
        self.exploded=False

        xleft=random.randint(-10,250)
        xright= random.randint(550,820)
        x=0
        if posLeft and posRight:
            x=random.choice([xleft,xright])
        elif posRight:
            x=xright
        elif posLeft:
            x=xleft

        self.flip=False
        if x==xright:
            self.flip= True

        self.position = (x, random.randint(100,400))
        self.speed = 0
        self.acc = 0
        self.turn = 0
        self.sentido = -1

        self.images = self.loadImages()
        self.imageIndex=0
        self.image = self.images[self.imageIndex %len(self.images)]
        self.rect = self.image.get_rect()
        #self.image = pygame.transform.rotate(self.image, -90)
        self.image = resizeImage(self.image,0.5)

    def update(self,bullets):
        self.imageIndex += 1
        self.image = self.images[self.imageIndex%len(self.images)]
        if time.time()>self.createTime+1*5:
            if random.randint(0,40)==1:
                x,y = self.position
                way=-1
                if self.flip:
                    way=1
                bullets.append(Bullet((x,y+22),90*way,3,'yellow'))
                bullets.append(Bullet((x,y+28),90*way,3,'yellow'))
        self.acc += self.ACCELERATION
        self.speed += self.acc

        if self.speed > self.TOP_SPEED:
            self.speed = self.TOP_SPEED
        if self.speed < 0:
            self.speed = 0
        if random.randint(0,50)==1:
            self.sentido *= -1
        x, y = self.position
        #x += self.speed * self.sentido
        y += self.speed * self.sentido
        if x <0:
            x = 0
        elif x>800-self.rect.width:
            x=800-self.rect.width
        y = y
        self.position = (x, y)
        self.image = self.image
        imgRect = self.image.get_rect()
        self.rect = pygame.Rect(self.position, (imgRect.width,imgRect.height))
    def loadImages(self):
        images = []
        for i in range(1,5):
            image = pygame.image.load(datafilepath('heli'+str(i)+'.png')).convert_alpha()
            image = resizeImage(image,0.4)
            if self.flip:
                image = pygame.transform.flip(image, True, False)
            images.append(image)
        return images

class Sound:
    def __init__(self,sound,vol):
        self.sound = pygame.mixer.Sound(sound)
        self.sound.set_volume(vol)
    def play(self): self.sound.play()

def createSounds():
    sounds = {}
    sounds['planecrash'] = Sound(datafilepath('planecrash.wav'),1)
    sounds['waitforbomb'] = Sound(datafilepath('waitforbomb.wav'),0.6)
    sounds['spill'] = Sound(datafilepath('spill.ogg'),1)
    sounds['bombfalling'] = Sound(datafilepath('bombfalling.wav'),1)
    sounds['bullet'] = Sound(datafilepath('bullet.wav'),0.8)
    sounds['tankdestroyed'] = Sound(datafilepath('tankdestroyed.wav'),1)
    sounds['miniexplosion'] = Sound(datafilepath('miniexplosion.wav'),0.5)
    sounds['dialogue'] = Sound(datafilepath('dialogue.wav'),0.3)
    sounds['man'] = Sound(datafilepath('man.ogg'),0.5)
    return sounds

def createSongs():
    songs = {}
    songs['fist'] = 'fist.mp3'
    songs['mind'] = 'mind.mp3'
    songs['cha'] = 'cha.mp3'
    songs['intro'] = 'intro.ogg'
    songs['missiona'] = 'missiona.ogg'
    songs['missionb'] = 'missionb.ogg'
    songs['dialoguea'] = 'dialoguea.ogg'
    songs['dialogueb'] = 'dialogueb.ogg'
    songs['ending'] = 'ending.ogg'
    return songs

def playSong(song,vol,loops):
        pygame.mixer.init()
        pygame.mixer.music.load(datafilepath(song))
        pygame.mixer.music.set_volume(vol)
        pygame.mixer.music.play(loops)

class TextLayer:
    def __init__(self,image, position):
        self.image=image
        self.position=position

def textOutlined(text,size,x,y, color='white'):
        layers=[]
        sombra = 1 #tamaño de la sombra
        fontObj = pygame.font.Font('freesansbold.ttf', size)

        # la sombra, creada duplicando el texto en negro
        tex1 = fontObj.render(text, True, (0,0,0))

        resultado = pygame.Surface((tex1.get_width()+(sombra*2),
                                        tex1.get_height()+(sombra*2)))

        resultado.set_alpha(0)
        resultado = resultado.convert_alpha()

        resultado.blit(tex1,(0,0))
        resultado.blit(tex1,(sombra*2,0))
        resultado.blit(tex1,(0,sombra*2))
        resultado.blit(tex1,(sombra*2,sombra*2))
##
##        layers += [TextLayer(tex1,(x-sombra,y-sombra))]
##        layers += [TextLayer(tex1,(x+sombra,y+sombra))]
##        layers += [TextLayer(tex1,(x+sombra,y-sombra))]
##        layers += [TextLayer(tex1,(x-sombra,y+sombra))]

        #el texto a mostrar
        textSurfaceObj = fontObj.render(text, True, THECOLORS[color])
        resultado.blit(textSurfaceObj,(sombra,sombra))
        return TextLayer(resultado,(x,y))

def resizeImage(image, multi):
    width = int(image.get_rect().width*multi)
    height= int(image.get_rect().height*multi)
    return pygame.transform.scale(image,(width, height))

def terminate():
    pygame.quit()
    sys.exit()

def deleteOldBullets(screen, bullets):
    infla=100
    rectAux = screen.get_rect()
    rectAux = rectAux.inflate(infla,infla)
    for bullet in bullets:
        if not rectAux.collidepoint(bullet.position):
            #colliderect doesn't work because pygame.draw never draw negatives
            bullets.remove(bullet)

def planeDeath(plane,explosions,sound):
            if not plane.exploded:
                sound.play()
                plane.exploded=True
                explosions.append(Explosion(plane.position))

def drawObjectsOnScreen(screen,objects):
    rects = []
    for elem in objects:
        _rect = screen.blit(elem.image,elem.position)
        rects.append(_rect)
    return rects

class DialogueEntity:
    def __init__(self,game,dialogue,pos=(50,50)):
        #print(dialogue)
        self.createTime = time.time()
        self.game=game
        self.dialogue=dialogue.split('|')
        self.pos=pos
    def update(self):
        if time.time()> self.createTime+0.1:
            thingsToDraw=[]
            #box and border
            x,y = self.pos
            wbox=self.game.width-x*2
            hbox=90
            pygame.draw.rect(self.game.screen,(50,50,50),((self.pos),(wbox,hbox)))
            border=pygame.draw.rect(self.game.screen, THECOLORS['blue'],((self.pos),(wbox,hbox)), 5)
            #dialogue
            thingsToDraw=[textOutlined(self.dialogue[2].strip('\n'),18,x+100,y+40)]
            drawObjectsOnScreen(self.game.screen,thingsToDraw)
            #picture
            distance= 5
            code=self.dialogue[1]
            if code=='pp':
                imagename='pilotpanic.png'
                xpic=border.right-90
            elif code=='pi':
                imagename='pilotinfected.png'
                xpic=border.right-90
            elif code=='de':
                imagename="doctorevil.png"
                xpic=x+distance
            elif code=='sg':
                imagename="secretguy.png"
                xpic=x+distance
            elif code=='ma':
                imagename="president.png"
                xpic=x+distance
            else:
                imagename="doctor.png"
                xpic=x+distance
            ypic=y+distance
            if code!='no':
                image = pygame.image.load(datafilepath(imagename)).convert_alpha()
                self.game.screen.blit(image,(xpic,ypic))

def datafilepath(filename):
    #return "data/"+filename
    return os.path.join('data', filename)

def read_dialogue(level):
    #read dialogues file
    f = open(datafilepath('dialogues.txt'))
    levelLines = f.readlines()
    f.close()
    lines=[]
    for line in levelLines:
        if line[0] == level:
            lines.append(line)
    return lines
    #draw dialogue box

def mission(game):
    mission_intro(game) #intro with tip

    FPSCLOCK = pygame.time.Clock()
    config= missionConfig(game.current_level)
    sounds = createSounds()
    allInfected = False
    gameover = False
    missionCompleted = False
    plane = Plane()
    plane.bombs=config.bombs
    bombHud = Bomb((game.width-50,20))
    explosions = []
    bombDroppped = False
    bullets= []
    gameoverReason = ""
    bomb = None

    ground = pygame.Rect(0, game.height-100,game.width, 100)
    groundSurf = pygame.Surface((game.width, 100))
    groundSurf = groundSurf.convert()
    groundSurf.fill(THECOLORS[config.groundColor])

    sky = pygame.Surface(game.screen.get_size())
    sky = sky.convert()
    sky.fill(THECOLORS[config.skyColor])

    background=None
    if config.bg_pic:
        background = pygame.image.load(datafilepath(config.bg_pic)).convert_alpha()
        background = resizeImage(background,1)

    imgHumans = ['man1.png', 'man2.png']
    humans = []
    for x in range(config.humans):
        humans.append(Human(random.choice(imgHumans)))

    tanks = []
    for x in range(config.tanks):
        tanks.append(Tank(config.vehiclesLeft,config.vehiclesRight))

    helicopters = []
    for x in range(config.helicopters):
        helicopters.append(Helicopter(config.vehiclesLeft,config.vehiclesRight))

    spills = []
    clouds = []

    for x in range(15):
        clouds.append(Cloud())

##    rocks = []
##    for i in range(100):
##        xrock=random.randint(-10,game.width+10)
##        yrock=random.randint(505,game.height+10)
##        rocks.append(pygame.Rect((xrock,yrock),(2,2)))

    bloods=[]

    songs = createSongs()

    if game.music_enabled:
        playSong(songs[config.songGame],0.6,-1)

    while True:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                terminate()
            elif event.type == pygame.KEYDOWN:
                if event.key == pygame.K_ESCAPE:
                    terminate()
                elif event.key in (K_RCTRL,K_LCTRL) and not plane.exploded:
                    sounds['bullet'].play()
                    bullets.append(Bullet(plane.position, plane.direction,
                                            plane.bulletSize,'red',True))
                elif event.key in (K_s, K_SPACE, K_RETURN):
                    if gameover or missionCompleted:
                        #game.state='mission'
                        return missionCompleted
                    elif not gameover and plane.bombReady and plane.bombs>0:
                        sounds['bombfalling'].play()
                        plane.bombs -= 1
                        plane.bombReady = False
                        bomb = Bomb(plane.position)
                        bombDroppped = True
                    elif not plane.bombReady:
                        sounds['waitforbomb'].play()

        if not plane.exploded:
            plane.update()

        if bombDroppped:
            #collision with the ground
            if bomb.position[1] > ground.centery-random.randint(-10,30):
                sounds['spill'].play()
                plane.bombReady = True
                spills.append(Spill(bomb.rect.center))
                bombDroppped=False
            else: #bomb continue descending
                bomb.update()

        #check if hummans contact with spills
        for spill in spills:
            if time.time() < spill.createTime+2:
                for hu in humans:
                    if spill.rect.colliderect(hu.rect):
                        hu.infected=1
                        hu.updateImage()
            else:
                spills.remove(spill)


        allInfected = True
        for human in humans:
            human.update(ground)
            if not human.infected:
                allInfected = False

        if spills == [] and plane.bombReady and (plane.bombs == 0
                                                and not allInfected):
            gameover = True
            gameoverReason = "You must infect all the humans, and you are out of bombs x-("

        if allInfected and helicopters==[] and tanks==[]:
            missionCompleted = True

        #plane can crash with the ground outside the screen
        if plane.position[1]+plane.rect.height > ground.top+10:
            gameover = True
            planeDeath(plane, explosions,sounds['planecrash'])

        for explosion in explosions:
            explosion.update(explosions)

        #collision bullet-helicopter
        for heli in helicopters:
            heli.update(bullets)
            for bullet in bullets:
                if bullet.rect.colliderect(heli.rect) and bullet.fromPlayer:
                    sounds['tankdestroyed'].play()
                    explosions.append(Explosion(heli.position))
                    bullets.remove(bullet)
                    helicopters.remove(heli)
                    break
        #crash plane-heli
        for heli in helicopters:
            if plane.rect.colliderect(heli.rect):
                    sounds['tankdestroyed'].play()
                    explosions.append(Explosion(heli.position))
                    helicopters.remove(heli)
                    gameover = True
                    planeDeath(plane, explosions,sounds['planecrash'])
                    break
        #collision bullet-tank
        for tank in tanks:
            tank.update(bullets)
            for bullet in bullets:
                if bullet.rect.colliderect(tank.rect) and bullet.fromPlayer:
                    sounds['tankdestroyed'].play()
                    explosions.append(Explosion(tank.position))
                    bullets.remove(bullet)
                    tanks.remove(tank)
                    break

        for bullet in bullets:
            bullet.update()
            for human in humans:
                if bullet.rect.colliderect(human.rect) and bullet.fromPlayer:
                    sounds['man'].play()
                    bloods.append(Blood(human.rect.center))
                    bullets.remove(bullet)
                    humans.remove(human)
                    gameover = True
                    gameoverReason = "Don't kill humans you @#&%, their minds are precious!"
                    break #necessary cause a bullet can hit more than one target
            if bullet.rect.colliderect(plane.rect) and not bullet.fromPlayer:
                gameover = True
                planeDeath(plane,explosions,sounds['planecrash'])


        #THINGS TO DRAW
        game.screen.blit(sky,(0,0))
        #ground and lines
        game.screen.blit(groundSurf,(0, game.height-100))
        if config.isRoad: #lines of the road
            lineLegth=80
            for i in range(0, game.width,lineLegth):
                pygame.draw.rect(game.screen, THECOLORS['white'],
                                    ((i,540),(lineLegth-20,7)))
##        if config.showRocks:
##            for rock in rocks:
##                pygame.draw.rect(game.screen, THECOLORS[config.rocksColor],rock)

        for cloud in clouds:
            game.screen.blit(cloud.image,cloud.position)

        if config.bg_pic:
            game.screen.blit(background,config.bg_pos)
        objectsToDraw=[]
        if not plane.exploded: objectsToDraw.append(plane)
        if bombDroppped: objectsToDraw += [bomb]
        objectsToDraw += spills
        objectsToDraw += bloods
        objectsToDraw += humans
        objectsToDraw += explosions
        objectsToDraw += helicopters
        objectsToDraw += tanks
        for bullet in bullets:
        	pygame.draw.rect(game.screen, THECOLORS[bullet.color],
        			             (bullet.position,(bullet.size,bullet.size)))
        objectsToDraw += [bombHud]

        texts=[]
        if gameover:
            texts += [textOutlined("GAMEOVER",32,200,220)]
            texts += [textOutlined(gameoverReason,16,200,250)]
            if round(time.time(), 0) % 2 == 1:
                texts += [textOutlined("Press Enter to play again",24,200,300)]

        if missionCompleted:
            texts += [textOutlined("Mission completed",32,200,250)]
            if round(time.time(), 0) % 2 == 1:
                texts += [textOutlined("Press Enter to continue",24,200,300)]

        if not plane.rect.colliderect(game.screen.get_rect()):
            texts += [textOutlined("Return to the battlefield!",
                                    32,200,170,'orange')]
            if plane.timeoutside==0: plane.timeoutside=time.time()
            if plane.timeoutside>0 and time.time() > plane.timeoutside+2.5:
                gameover = True
                gameoverReason = "Mission aborted. You left the battlefield!"
        else:
            plane.timeoutside = 0

        objectsToDraw += texts

        objectsToDraw += [textOutlined(str(plane.bombs),10,game.width-30,23)]

        rects = drawObjectsOnScreen(game.screen,objectsToDraw) #DRAW ALL THE OBJECTS

        pygame.display.update()
        FPSCLOCK.tick(game.fps)

        deleteOldBullets(game.screen, bullets)

        #mostrar FPS
        #pygame.display.set_caption("FPS: %.2f" % (FPSCLOCK.get_fps()))

def intro(game):
    pygame.mixer.music.stop()
    FPSCLOCK = pygame.time.Clock()
    inicio = time.time()
    index=0 #for dialogues
    dialogues = read_dialogue("0")
    dialogue= DialogueEntity(game,dialogues[index])
    ground = pygame.Rect(0, game.height-100,game.width, 100)
    sounds = createSounds()
    planename='cargoplane.png'
    plane=Plane(planename)
    planeAfterCrash=pygame.image.load(datafilepath(planename)).convert_alpha()
    planeAfterCrash= pygame.transform.rotate(planeAfterCrash, 25)
    plane.position=(-100,-50)
    plane.MIN_SPEED=5
    plane.direction=-125
    timeBeforeInfection = 0

    songs = createSongs()

    human = Human('man1.png')
    human.update(ground)
    human.image = pygame.image.load(datafilepath('man1.png')).convert_alpha()
    human.image = pygame.transform.rotate(human.image, 120)
    rect = human.image.get_rect()
    human.rect = pygame.Rect(human.position,(rect.width,rect.height))

    explosions=[]
    spills=[]

    clouds = []
    for x in range(15):
        clouds.append(Cloud())

    while True:
        for event in pygame.event.get(): # event handling loop
            if event.type == QUIT:
                terminate()

            elif event.type == KEYDOWN:
                if event.key in (K_s,K_SPACE, K_RETURN):
                    #game.state = 'mission'
                    return
                elif event.key == K_ESCAPE:
                    terminate()

        game.screen.fill(THECOLORS['violet'])
        for cloud in clouds: game.screen.blit(cloud.image, cloud.position)
        pygame.draw.rect(game.screen, THECOLORS['salmon4'], ground)

        if inicio+4>time.time()>inicio+2:
            if index != 1:
                index=1
                dialogue= DialogueEntity(game,dialogues[index])
        if inicio+6>time.time()>inicio+4:
            if index !=2:
                index=2
                dialogue= DialogueEntity(game,dialogues[index])
            if explosions==[] and not plane.exploded:
                sounds['miniexplosion'].play()
                x,y=plane.rect.center
                y=random.randint(y-10,y+10)
                explosions.append(Explosion((x,y),0.3))

        if plane.position[1]+plane.rect.height > ground.top and not plane.exploded:
            timeBeforeInfection=time.time()
            planeDeath(plane,explosions,sounds['planecrash'])
            sounds['spill'].play()
            x,y=plane.rect.center
            spill = Spill((x+50,y+90))
            spill.image = resizeImage(spill.image,0.3)
            rectspill = spill.image.get_rect()
            spill.rect = pygame.Rect(spill.position,(rectspill.width,rectspill.height))
            spills.append(spill)

        for spill in spills:
            game.screen.blit(spill.image, spill.position)
            if spill.rect.colliderect(human.rect):
                if time.time()>timeBeforeInfection+2:
                    human.infected=1
                    if not time.time()>timeBeforeInfection+6:
                        human.updateImage()

        if not plane.exploded:
            plane.update(False)
            game.screen.blit(plane.image, plane.position)
        else:
            if game.music_enabled and not pygame.mixer.music.get_busy():
                playSong(songs['intro'],0.4,-1)
            x,y = plane.position
            game.screen.blit(planeAfterCrash,(x,y+20))
            #the pilot
            human.position=(x+30,y+80)
            game.screen.blit(human.image,human.position)
            rect = human.image.get_rect()
            human.rect = pygame.Rect(human.position,(rect.width,rect.height))

        for explosion in explosions:
            explosion.update(explosions)
            game.screen.blit(explosion.image, explosion.position)

        texts=[]
        if not plane.exploded:
            dialogue.update()

        if timeBeforeInfection!=0:
            if timeBeforeInfection+6<time.time()<timeBeforeInfection+9:
                if index!=3:
                    index = 3
                    dialogue= DialogueEntity(game,dialogues[index])
                dialogue.update()
                human.movex=10
                human.movey=5
                human.infected = False
                human.update(ground)
            if time.time()>timeBeforeInfection+11:
                texts+= [textOutlined("Brain",72,150,210,'chartreuse')]
            if time.time()>timeBeforeInfection+12:
                texts+=[textOutlined("Brainwashed",72,150,210,'chartreuse')]
            if time.time()>timeBeforeInfection+13:
                texts+= [textOutlined("apadentro 2012",10,10,555)]
                texts+= [textOutlined("v1.0",10,10,570)]
            if time.time()>timeBeforeInfection+15:
                if round(time.time(), 0) % 2 == 1:
                    texts += [textOutlined("Press Enter to continue",24,200,300)]

        drawObjectsOnScreen(game.screen,texts)

        pygame.display.update()
        FPSCLOCK.tick(game.fps)

def dialogueScene(game):
    FPSCLOCK = pygame.time.Clock()
    inicio = time.time()
    sounds = createSounds()
    dialogues = read_dialogue(str(game.current_level))
    index=0 #for dialogues
    dialogue=DialogueEntity(game,dialogues[index])

    config=missionConfig(game.current_level)

    songs = createSongs()
    if game.music_enabled:
        playSong(songs[config.songDialogue],0.3,-1)

    background=None
    if config.bg_pic:
        background = pygame.image.load(datafilepath(config.bg_pic)).convert_alpha()
        background = resizeImage(background,1)

    clouds = []
    for x in range(15):
        clouds.append(Cloud())


    while True:
        for event in pygame.event.get(): # event handling loop
            if event.type == QUIT:
                terminate()

            elif event.type == KEYDOWN:
                if event.key == K_s:
                    #game.state = 'mission'
                    return
                elif event.key in (K_SPACE, K_RETURN):
                    index += 1
                    if index>=len(dialogues):
                        #game.state = 'mission'
                        return
                    else:
                        sounds['dialogue'].play()
                        texto=dialogues[index].split('|')
                        if texto[2]=='>=)\n':#no music for ending dialogues
                            pygame.mixer.music.fadeout(2)
                        dialogue=DialogueEntity(game,dialogues[index])
                elif event.key == K_ESCAPE:
                    terminate()
        game.screen.fill(THECOLORS[config.skyColor])

        for cloud in clouds: game.screen.blit(cloud.image, cloud.position)

        ground = pygame.Rect(0, game.height-100,game.width, 100)
        pygame.draw.rect(game.screen, THECOLORS[config.groundColor], ground)

        if config.bg_pic:
            game.screen.blit(background,config.bg_pos)

        dialogue.update()

        texts=[]
        dist=30
        texts+=[textOutlined("Press 's' to skip",12,dist,game.height-dist)]
        texts+=[textOutlined("Press 'space' for next dialogue",12,580,game.height-dist)]

        drawObjectsOnScreen(game.screen,texts)
        pygame.display.update()
        FPSCLOCK.tick(game.fps)

##################################
## MISSSION INTRO
##################################
def mission_intro(game):
    config = missionConfig(game.current_level)
    FPSCLOCK = pygame.time.Clock()
    inicio = time.time()
    while True:
        for event in pygame.event.get(): # event handling loop
            if event.type == QUIT:
                terminate()

            elif event.type == KEYDOWN:
                if event.key in (K_s, K_SPACE, K_RETURN):
                    #game.state = 'mission'
                    return
                elif event.key == K_ESCAPE:
                    terminate()

        game.screen.fill((0,0,0))
        texts=[]
        if game.current_level==5:
            texts+=[textOutlined("The End",60,250,210)]
            texts+=[textOutlined("Thanks for playing.",30,250,270)]
        else:
            texts+=[textOutlined("Mission "+str(game.current_level) +":",60,250,210)]
            texts+=[textOutlined(config.mission_subtitle,30,250,270)]
            #tip of the mission
            texts+=[textOutlined("Tip: "+config.mission_tip,16,250,400)]

        if time.time() > inicio+2:
            if round(time.time(), 0) % 2 == 1:
                texts += [textOutlined("Press Enter to continue",
                                        24,250,500,'orange')]

        drawObjectsOnScreen(game.screen,texts)

        pygame.display.update()
        FPSCLOCK.tick(game.fps)

def menu_select_mission(game):
    config = missionConfig(game.current_level)
    FPSCLOCK = pygame.time.Clock()
    inicio = time.time()

    missionList=[]
    missionList+=["Mission 1: The Beach"]
    missionList+=["Mission 2: MindRa surroundings"]
    missionList+=["Mission 3: Military Base"]
    missionList+=["Mission 4: Final Battle at MindRa"]

    level = game.current_level+1
    if level > 4:
        level = 4

    positions = []
    x,y =(200,200)
    for i in range(0,level):
        positions.append((x,y))
        y+=50

    index = 0

    while True:
        for event in pygame.event.get(): # event handling loop
            if event.type == QUIT:
                terminate()

            elif event.type == KEYDOWN:
                if event.key in (K_s, K_SPACE, K_RETURN):
                    game.current_level=index%level
                    return
                elif event.key == K_UP:
                    index -=1
                elif event.key == K_DOWN:
                    index +=1
                elif event.key == K_ESCAPE:
                    terminate()

        game.screen.fill((0,0,0))
        texts=[]
        texts+=[textOutlined("Select Mission",60,150,50)]

        for i in range(0,level):
            x,y=positions[i]
            texts+=[textOutlined(missionList[i],24,x,y)]

        xbor, ybor= x,y=positions[index%level]
        border=pygame.draw.rect(game.screen, THECOLORS['green'],
                                    ((xbor-7,ybor-7),(500,40)), 3)

        if time.time() > inicio+2:
            if round(time.time(), 0) % 2 == 1:
                texts += [textOutlined("Press Enter to continue",
                                        24,250,500,'orange')]

        drawObjectsOnScreen(game.screen,texts)

        pygame.display.update()
        FPSCLOCK.tick(game.fps)

def missionConfig(index):
    config = MissionConfig()
    if index==1:    #mission1: beach
        config.mission_subtitle="Testing the ooze"
        config.mission_tip="use space bar to drop a bomb."
        config.helicopters=0
        config.tanks=0
        config.humans= 20
        config.bombs=4
        config.skyColor='lightskyblue'
        config.groundColor='yellow'
        config.rocksColor='brown'
        config.isRoad= False
        config.vehiclesLeft=False
        config.vehiclesRight=False
        config.showRocks=False
        config.bg_pic=None
        config.bg_pos=None
        config.songDialogue='dialoguea'
        config.songGame='missiona'
    elif index==2:    #mission2: road
        config.mission_subtitle="Defend MindRa"
        config.mission_tip="use control keys to use the machine gun."
        config.helicopters=6
        config.tanks=8
        config.humans= 6
        config.bombs=3
        config.skyColor='lightskyblue3'
        config.groundColor='dimgrey'
        config.rocksColor='brown'
        config.isRoad= True
        config.vehiclesLeft=False
        config.vehiclesRight=True
        config.showRocks=False
        config.bg_pic='mindra.png'
        config.bg_pos=(-400,200)
        config.songDialogue='dialogueb'
        config.songGame='missionb'
    elif index==3:    #mission3: military base
        config.mission_subtitle="Counterattack"
        config.mission_tip="attack the helicopters from above."
        config.mission_tip="destroy the vehicles before they start shooting."
        config.helicopters=10
        config.tanks=2
        config.humans= 30
        config.bombs=6
        config.skyColor='deepskyblue4'
        config.groundColor='tan3'
        config.rocksColor='brown'
        config.isRoad= False
        config.vehiclesLeft=True
        config.vehiclesRight=True
        config.showRocks=False
        config.bg_pic='militarybase.png'
        config.bg_pos=(100,235)
        config.songDialogue='missiona'
        config.songGame='missiona'
    elif index==4:    #mission4: MindRa HQ
        config.mission_subtitle="Final battle at MindRa"
        config.mission_tip="survive."
        config.helicopters=8
        config.tanks=12
        config.humans= 0
        config.bombs=3
        config.skyColor='lightskyblue2'
        config.groundColor='dimgrey'
        config.rocksColor='brown'
        config.isRoad= False
        config.vehiclesLeft=True
        config.vehiclesRight=True
        config.showRocks=False
        config.bg_pic='mindra.png'
        config.bg_pos=(100,200)
        config.songDialogue='missionb'
        config.songGame='missionb'
    elif index==5:    #Ending
        config.mission_subtitle="Final battle at MindRa"
        config.mission_tip="survive."
        config.helicopters=8
        config.tanks=10
        config.humans= 0
        config.bombs=3
        config.skyColor='lightskyblue2'
        config.groundColor='dimgrey'
        config.rocksColor='brown'
        config.isRoad= False
        config.vehiclesLeft=True
        config.vehiclesRight=True
        config.showRocks=False
        config.bg_pic='mindra.png'
        config.bg_pos=(100,200)
        config.songDialogue='ending'
        config.songGame='missionb'
    return config

def read_savegame(filename):
    savegame = shelve.open(filename)
    level = 0
    if 'level' in savegame:
        level = savegame['level']
    #print(level)
    return level

def store_savegame(filename,level):
    oldlevel=read_savegame(filename)
    savegame = shelve.open(filename)
    if level > oldlevel:
        savegame['level']=level
    savegame.close()


def game():
    pygame.init()
    pygame.display.set_caption('Brainwashed')

    #quitar humanos grises, ubicacion vehiculos, jefe final, escenarios.

    game = GameConfig()
    game.fps = 31
    game.width = 800
    game.height = 600
    #game.state = 'dialogue'
    game.screen=  pygame.display.set_mode((game.width, game.height))
    game.music_enabled = True

    savegame_filename = 'savegame.dat'
    game.current_level=read_savegame(savegame_filename)

    intro(game)
    if game.current_level>0:
        menu_select_mission(game)

    missionCompleted=True #so we can see the initial dialogue the first time

    while game.current_level in range(0,5):
        if missionCompleted:
            store_savegame(savegame_filename,game.current_level)
            game.current_level += 1
            if game.current_level == 5: break
            dialogueScene(game)
        missionCompleted = mission(game)

    dialogueScene(game)
    mission_intro(game)
    return #return to main menu

def main():
    while True:
        game()

if __name__ == '__main__':
    main()
    cProfile.run('main()')
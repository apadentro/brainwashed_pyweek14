Brainwashed
===============
"Your name is Lizzo, a skilled pilot who is assign to dubious tasks after an accident.Your name is Lizzo, a skilled pilot who is assign to dubious tasks after an accident."

Complete the 4 missions to see the ending!

Entry in PyWeek #14  <http://www.pyweek.org/14/>
Team: apadentro Soft
Members: apadentro
Last update: 12/05/2012

#REQUIREMENTS:
	Works with Python 3.2 and 2.7.2
	PyGame 1.9.2pre:	http://www.pygame.org/

#DEVELOPING TOOLS:
	Operating System: 	Windows XP
	Python 3.2.2:		http://www.python.org/
	PyGame 1.9.2pre:	http://www.pygame.org/

#RUNNING THE GAME:
On Windows or Mac OS X, execute: 
	python run_game.py

#HOW TO PLAY THE GAME:
	Use arrow keys or (AWSD) for moving
	UP ARROW - boost
	UP ARROW - decelerate
	Space - drop bomb
	Control keys - machine gun
	
	Hud keys:
		Enter, space - Next or continue
		s - skip

#LICENSE:
##Code
	Source code (.py) under the GPL v3.
	http://www.gnu.org/licenses/gpl.html

##Media	
	Pictures created with Paint.NET
	Sounds created with http://www.bfxr.net/
	The rest of the media (pictures, sounds, music) was created by me. IT is under CC�by�sa�3.0.
	http://creativecommons.org/licenses/by-sa/3.0/deed.es
	
	Pictures by other people
	cloud http://www.iconfinder.com/icondetails/65751/40/cloud_icon
	plane http://www.iconfinder.com/icondetails/34849/24/airplane_tourism_icon
	plane landing http://www.iconfinder.com/icondetails/65258/128/367_icon
	bomb http://www.iconfinder.com/icondetails/65474/128/bomb_war_icon
	Helicopter http://www.iconfinder.com/icondetails/41962/128/helicopter_icon	
	tank http://www.iconfinder.com/icondetails/65355/128/218_icon
	man1 http://www.iconfinder.com/icondetails/65355/128/218_icon
	man2 http://www.iconfinder.com/icondetails/51528/26/man_user_icon
	
	Music from other people:
	"Maestro Off The Chain"		http://www.newgrounds.com/audio/listen/484660
	"Welcome To Fist" 			http://www.newgrounds.com/audio/listen/478645
	"Burned On My Mind - 8bit" 	http://www.newgrounds.com/audio/listen/484382
	
#CHANGELOG
1.3 (23 may 2012)
TextOutlined improved. Now just returns a surface.
Blit surfaces for the sky and the ground instead of filling the game screen with a color.
1.2 (18 may 2012)
Removed images chache for class Tank and Helicopter because of flipping problem
1.1 (15 may 2012)
Images chache for class Tank and Helicopter
convert_alfa() added when loading pictures
1.0 (12 may 2012)
Initial version